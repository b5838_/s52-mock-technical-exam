function countLetter(letter, sentence) {
    let result = 0;
    // Check first whether the letter is a single character.
    // If letter is a single character
    // count how many times a letter has occurred in a given sentence then return count.
     // If letter is invalid, return undefined.
     if(letter.length === 1 && typeof letter === "string"){
        for(let i = 0; i < sentence.length; i++){
            if(letter === sentence[i]){
                result += 1;
            }         
        }   
    }
    else{
        result = undefined;
    }

    return result;
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    // It disregard the text casing
    let smallText = text.toString();
    let result;

    // smallText = hello
    // 1st slice[0 + 1] = ello, check if h includes in ello (false)
    // 2nd slice[1 + 1] = llo, check if e includes in llo (false)
    // 3rd slice[2 + 1] = lo, check if l includes in lo (true) break;
        // if(true) return false
    for (textIndex = 0; textIndex < smallText.length; textIndex++){
        if(smallText.slice(textIndex + 1).includes(smallText[textIndex])){
            result = false;
            break;
        }
        else{
            result = true;
        }
    }

    return result;
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    let discountedPrice = (price - (price * 0.2));
    let roundedPrice = discountedPrice.toFixed(2).toString();

    if(age < 13) {
        return undefined;
    }
    else if(age <= 21){
        return roundedPrice;
    }
    else if(age <= 64){
        return price.toFixed(2).toString();
    }
    else{
        return roundedPrice;
    }

}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    const result = [];
    const zeroStock = items.filter(item => item.stocks == 0);
    zeroStock.forEach(item => {
        if(result.includes(item.category)){
            return;
        }
        else{
            result.push(item.category);
        }
    });
    return result;
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    // const sameCandidate = [];

    // candidateA.forEach(candidate => {
    //     if(candidateB.includes(candidate)){
    //         sameCandidate.push(candidate);
    //     }
    // })

    // return sameCandidate;

    const same = candidateA.filter(candidate => candidateB.includes(candidate))

    return same;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};